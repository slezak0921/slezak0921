package com.epam.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.epam.web.view.HotelSummaryViewTransformer;
import com.epam.web.view.ModelHotelSummary;

@Controller
public class HotelController {
	@Autowired
	private HotelSummaryViewTransformer hotelTransformer;

	@ModelAttribute("modelHotelSummary")
	public ModelHotelSummary modelHotelSummary(HotelIdRequest hotelIdRequest) {
		ModelHotelSummary modelHotelSummary = new ModelHotelSummary(
				hotelTransformer.getMotelHotelSummary(hotelIdRequest.getHotelId()));
		return modelHotelSummary;
	}
	
	@RequestMapping("/searchHotelId")
	public String shearchHoltelId(){
		return "hotels";
	}
}
