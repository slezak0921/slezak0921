package com.epam.web.view;

import java.util.List;

public class ModelHotelSummary {
	private List<HotelSummaryView> hotelSummaryViews;
	
	public ModelHotelSummary(List<HotelSummaryView> hotelSummaryViews) {
		super();
		this.hotelSummaryViews = hotelSummaryViews;
	}

	public List<HotelSummaryView> getHotelSummaryViews() {
		return hotelSummaryViews;
	}
}
