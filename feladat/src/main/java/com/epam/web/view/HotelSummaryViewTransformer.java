package com.epam.web.view;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.domain.Feedback;
import com.epam.service.HotelSearcher;

@Component
public class HotelSummaryViewTransformer {
	@Autowired
	private HotelSearcher hotelSearcher;
	
	public List<HotelSummaryView> getMotelHotelSummary(long hotelId){
		List<HotelSummaryView> hotelSummaryViews = new ArrayList<>();
		for(Feedback feedback : hotelSearcher.giveTheResultToUser(hotelId)){
			hotelSummaryViews.add(transFormFeedbackToSummaryView(feedback));
		}
		hotelSearcher.clearList();
		return hotelSummaryViews;
	}
	
	private HotelSummaryView transFormFeedbackToSummaryView(Feedback feedback){
		HotelSummaryView hotelSummaryView = new HotelSummaryView();
		hotelSummaryView.setFeedback(feedback.getFeedback());
		hotelSummaryView.setStar(feedback.getStar());
		return hotelSummaryView;
	}
}
