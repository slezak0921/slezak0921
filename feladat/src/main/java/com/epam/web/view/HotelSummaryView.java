package com.epam.web.view;

public class HotelSummaryView {
	private String feedback;
	private long star;
	
	public String getFeedback() {
		return feedback;
	}
	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}
	public long getStar() {
		return star;
	}
	public void setStar(long star) {
		this.star = star;
	}
}
