package com.epam.web;

public class HotelIdRequest {
	private long hotelId;

	public long getHotelId() {
		return hotelId;
	}

	public void setHotelId(long hotelId) {
		this.hotelId = hotelId;
	}
}
