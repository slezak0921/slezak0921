package com.epam.web;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class HomeController {
	private final static String  REQUEST_MAPPING = "/";	
	
	@ModelAttribute("hotelIdRequest")
	public HotelIdRequest sendHotelIdRequest() {
		return new HotelIdRequest();
	}
	
	@RequestMapping(REQUEST_MAPPING)
	public String home(RedirectAttributes redirectAttributes) {
		redirectAttributes.addFlashAttribute("message", String.format("Invalid hotelID!"));
		return "home";
	}	
}
