package com.epam.domain;

public class Feedback {
	private long id;
	private String feedback;
	private long star;
	private long hotelId;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getFeedback() {
		return feedback;
	}
	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}
	public long getStar() {
		return star;
	}
	public void setStar(long star) {
		this.star = star;
	}
	public long getHotelId() {
		return hotelId;
	}
	public void setHotelId(long hotelId) {
		this.hotelId = hotelId;
	}
	
}
