package com.epam.feladat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages="com.epam")
@SpringBootApplication
public class FeladatApplication {

	public static void main(String[] args) {
		SpringApplication.run(FeladatApplication.class, args);
	}
}
