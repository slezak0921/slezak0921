package com.epam.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.domain.Feedback;
import com.epam.service.strategies.CsvFileReader;
import com.epam.service.strategies.FileReaderStrategy;
import com.epam.service.strategies.JsonFileReader;
import com.epam.service.strategies.XmlFileReader;

@Service
public class FileReader {
	private List<FileReaderStrategy> fileReaderStrageies = new ArrayList<>();
	@Autowired
	private CashFeedback cashFeedback;
	
	private final static String JSON_FILE_PATH = "src/main/resources/static/IndependentAuditor.json";
	private final static String CSV_FILE_PATH = "src/main/resources/static/HotelRoomReseller.csv";
	private final static String XML_FILE_PATH = "src/main/resources/static/ThirdPartyDataStore.xml";

	public List<Feedback> excecuteStrategies(List<Feedback> feedbacks) {
		if(fileReaderStrageies.isEmpty()) {
			addStrategiesToList();
			for (FileReaderStrategy strategy : fileReaderStrageies) {
				strategy.readFile(feedbacks);
	
			}
			cashFeedback.setFeedbacks(feedbacks);
		}
		return cashFeedback.getFeedbacks();
	}

	private void addStrategiesToList() {
		fileReaderStrageies.add(new JsonFileReader(JSON_FILE_PATH));
		fileReaderStrageies.add(new XmlFileReader(XML_FILE_PATH));
		fileReaderStrageies.add(new CsvFileReader(CSV_FILE_PATH));
	}
}
