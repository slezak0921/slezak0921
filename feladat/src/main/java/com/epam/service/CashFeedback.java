package com.epam.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.epam.domain.Feedback;

 @Component
public final class CashFeedback {
	private List<Feedback> feedbacks = new ArrayList<>();

	public void setFeedbacks(List<Feedback> feedbacks) {
		this.feedbacks = feedbacks;
	}

	public List<Feedback> getFeedbacks() {
		return feedbacks;
	}

}
