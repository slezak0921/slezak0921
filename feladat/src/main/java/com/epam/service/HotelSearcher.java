package com.epam.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.dao.FeedbackDao;
import com.epam.domain.Feedback;

@Service
public class HotelSearcher {
	@Autowired
	private FeedbackDao feedbackDao;
	@Autowired
	private FileReader fileReader;
	private List<Feedback> feedbacks = new ArrayList<>();
	
	public List<Feedback> giveTheResultToUser(long hotelId) {
		feedbacks = feedbackDao.getHotelFeedbacksById(hotelId, readFeedbacksFromFiles(feedbacks));
		return feedbacks;
	}

	private List<Feedback> readFeedbacksFromFiles(List<Feedback> feedbacks) {
		return fileReader.excecuteStrategies(feedbacks);
	}
	
	public void  clearList() {
		this.feedbacks.clear();
	}
}
