package com.epam.service.strategies;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.epam.domain.Feedback;

public class JsonFileReader implements FileReaderStrategy {
	private String path;
	
	public JsonFileReader(String path) {
		super();
		this.path = path;
	}

	@Override
	public List<Feedback> readFile(List<Feedback> feedbacks) {
		JSONParser parser = new JSONParser();
		try {
			Object datas =  parser.parse(new FileReader(path));
			JSONObject jsonObject = (JSONObject) datas;
			JSONArray jsonFeedbacks = (JSONArray) jsonObject.get("feedbacks");
			for(Object data : jsonFeedbacks){
				JSONObject jsonFeedback = (JSONObject) data;
				Feedback feedback = createFeedback(jsonFeedback);
				feedbacks.add(feedback);
			}
		} catch (IOException | ParseException e) {
			e.printStackTrace();
		}   
		return feedbacks;
	}

	private Feedback createFeedback(JSONObject jsonFeedback) {
		Feedback feedback = new Feedback();
		feedback.setId((long) jsonFeedback.get("id"));
		feedback.setFeedback((String) jsonFeedback.get("feedback"));
		feedback.setStar((long) jsonFeedback.get("rank"));
		feedback.setHotelId((long) jsonFeedback.get("hotelId"));
		return feedback;
	}

}
