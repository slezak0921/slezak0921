package com.epam.service.strategies;

import java.util.List;

import com.epam.domain.Feedback;

public interface FileReaderStrategy {
	List<Feedback> readFile(List<Feedback> feedbacks);
}
