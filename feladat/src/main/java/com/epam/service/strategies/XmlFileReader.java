package com.epam.service.strategies;

import java.io.File;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.epam.domain.Feedback;
import com.epam.jaxb.Reviews;

public class XmlFileReader implements FileReaderStrategy{
	private String path;
	
	public XmlFileReader(String path) {
		super();
		this.path = path;
	}

	@Override
	public List<Feedback> readFile(List<Feedback> feedbacks) {
		File file = new File(path);
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(Reviews.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			Reviews reviews = (Reviews) jaxbUnmarshaller.unmarshal(file);
			for(Reviews.Review review : reviews.getReview()){
				Feedback feedback = createFeedback(review);
				feedbacks.add(feedback);
			}
		} catch (JAXBException e) {
			e.printStackTrace();
		}	
		return feedbacks;
	}

	private Feedback createFeedback(Reviews.Review review) {
		Feedback feedback = new Feedback();
		feedback.setId(review.getId());
		feedback.setFeedback(review.getDescription());
		feedback.setHotelId(review.getHotelId());
		feedback.setStar(review.getRank());
		return feedback;
	}
}
