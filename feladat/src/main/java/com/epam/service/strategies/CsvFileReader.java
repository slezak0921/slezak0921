package com.epam.service.strategies;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;

import com.epam.domain.Feedback;

public class CsvFileReader implements FileReaderStrategy{
	private static final String CSV_SEPARATED_CHAR = ",";
	private String path;
	private int sizeOfReadData;
	
	public CsvFileReader(String path) {
		super();
		this.path = path;
	}

	
	@Override
	public List<Feedback> readFile(List<Feedback> feedbacks) {
		 try {
			Scanner scan = new Scanner(new File(path));
			while (scan.hasNextLine()) {
		        String line = scan.nextLine();
		        String[] lineArray = line.split(CSV_SEPARATED_CHAR);
		        sizeOfReadData = lineArray.length;
		        Feedback feedback = createFeedback(lineArray);
		        feedbacks.add(feedback);
		    }
			scan.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return feedbacks;
	}

	private Feedback createFeedback(String[] lineArray) {
		Feedback feedback = new Feedback();
		feedback.setId(Long.valueOf(lineArray[0]));
		feedback.setFeedback(lineArray[1]);
		if(sizeOfReadData == 4){
			feedback.setStar(Long.valueOf(lineArray[2]));
			feedback.setHotelId(Long.valueOf(lineArray[3]));
		}else{
			feedback.setStar(Long.valueOf(0));
			feedback.setHotelId(Long.valueOf(lineArray[2]));
		}	
		return feedback;
	}

}
