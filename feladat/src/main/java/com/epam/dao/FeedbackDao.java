package com.epam.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.epam.domain.Feedback;

@Repository
public class FeedbackDao{
	private List<Feedback> feedbacks = new ArrayList<>();
	
	public List<Feedback> getHotelFeedbacksById(long hotelId, List<Feedback> givenFeedbacks){
		feedbacks = givenFeedbacks.stream()
		.filter(feedback -> feedback.getHotelId() == hotelId)
		.sorted((Feedback o1, Feedback o2) -> Long.valueOf(o2.getStar()).compareTo(Long.valueOf(o1.getStar())))
		.collect(Collectors.toList());
		return feedbacks;
	}
}
